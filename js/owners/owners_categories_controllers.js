/*
* ownerCategoriesMainCtrl Controller
*/
function ownersCategoriesMainCtrl($http, $scope){
	var req = {
		method: 'GET', 
		url: "http://"+ $scope.url + ":" + $scope.port + "/owner_categories"
	}
	$http(req).then(function successCallback(response){
		$scope.categoriesCollection = response.data.collection;
	});
}
/*
* usersCategoriesNewCtrl Controller
*/
function ownersCategoriesNewCtrl($http, $scope){
	$scope.successMsg = false;
	$scope.errorMsg = false;
	this.newCategory = function newCategory(){
		var req = {
			method: 'POST',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owner_categories",
			data: {
				"owner_category":{
					"name": $scope.category_name,
					"description": $scope.category_description
				}
			}
		}
		$http(req).then(function successCallback(response){
			$scope.successMsg = true;
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}
}
/*
* ownersCategoriesEditCtrl Controller
*/
function ownersCategoriesEditCtrl($http, $scope, $stateParams){
	$scope.successMsg = false;
	$scope.errorMsg = false;
	var req = {
		method: 'GET', 
		url: "http://"+ $scope.url + ":" + $scope.port + "/owner_categories/" + $stateParams.id
	}
	$http(req).then(function successCallback(response){
		$scope.category_name = response.data.owner_category.name;
		$scope.category_description = response.data.owner_category.description;
	});
	this.editCategory = function editCategory(){
		var req = {
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owner_categories/" + $stateParams.id,
			data: {
				"owner_category":{
					"name": $scope.category_name,
					"description": $scope.category_description
				}
			}
		}
		$http(req).then(function successCallback(response){
			$scope.successMsg = true;
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}
}
/* Attachments */
angular
    .module('inspinia')
    .controller('ownersCategoriesMainCtrl', ownersCategoriesMainCtrl)
    .controller('ownersCategoriesNewCtrl', ownersCategoriesNewCtrl)
    .controller('ownersCategoriesEditCtrl', ownersCategoriesEditCtrl)