/*
* ownersMainCtrl Controller
*/
function ownersMainCtrl($scope, $http){
	$scope.ownersCollection = '';
	
	$http({
		method:'GET',
		url: "http://"+ $scope.url + ":" + $scope.port + "/owners"
	}).then(function successCallback(response){
		$scope.ownersCollection = response.data.collection;
	});

	this.searchOwner = function searchOwner(country_id){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners/search_by_country_id/" + country_id
		}).then(function successCallback(response){
			window.location.href = "#/owners/owners_edit/"+response.data.owner.id+"/step_one";
		}, function errorCallback(response){	
			alert("Propietario no encontrado.")
		});
	}
	

	this.blockOwner = function blockOwner(idOwner, active){
		var blockReq = {
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners/" + idOwner,
			data: {
				"owner":{
					"active": !active
				}
			}
		}
		$http(blockReq).then(function successCallback(response){
			$scope.ownersCollection[response.data.owner.id - 1].active = response.data.owner.active;
		}, function errorCallback(response){
			alert("Error");
		});
	}
}
/**
* New Owner Controller - Send data to api to create new owner in database.
**/
function ownersNewCtrl($scope, $http, ownerCategories){
	$scope.registeredPlate = false;
	$scope.plateInDb = false;
	$scope.checkUniquenessInput = false;
	$scope.invalidData = false;
	$scope.disabledSubmit = true;
	$scope.password = "";
	$scope.confirmation = "";
	$scope.ownerCategoriesData = "";
	$scope.plans = "";
	$scope.new = {};
	$scope.new.plates = [];

	$scope.$watchGroup(['newForm.$valid', 'newForm.$dirty', 'checkUniquenessInput'], function(value){

		if(value[0] && value[1] && !value[2]){
			$('#step2Li').css('cursor','pointer');
			$('#step2Btn').css('pointer-events','auto');
		}
		if((!value[0] && value[1]) || !value[1] || value[2]){
			$('#step2Li').css('cursor','not-allowed');
			$('#step2Btn').css('pointer-events','none');
		}
	});

	$scope.$watch('new.plates.length', function(value){
		if(value > 0){
			$scope.disabledSubmit = false;
		}
		if(value == 0){
			$scope.disabledSubmit = true;
		}
	});

	/* Load category of owners */
	ownerCategories.async().then(function(response){
		$scope.ownerCategoriesData = response.data.collection;
		$scope.ownerCategoriesData.selected = $scope.ownerCategoriesData[0];
	});

	this.change = function change(){
		$scope.invalidData = false;
		$scope.registeredPlate = false;
		$scope.plateInDb = false;
		$scope.successMsg = false;
		$scope.errorMsg = false;
	}
	/* Add plates to table and json data */
	this.addPlate = function addPlate(plate){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plates/check_uniqueness/plate_char",
			params: { 'plate[plate_chars]': plate }
		}).then(function successCallback(response){
			$scope.index_of = $scope.new.plates.indexOf(plate);
			if($scope.index_of >= 0){
				$scope.invalidData = true;
			}else if(!response.data){
				$scope.plateInDb = true;
			}
			else{
				$scope.new.plates.push(plate);			
			}
			$('#plate_chars').val('');
			$scope.new.plate_chars = "";		
		}, function errorCallback(response){

		});
	}

	this.removePlate = function removePlate(plate){
		$scope.index_of = $scope.new.plates.indexOf(plate);
		$scope.new.plates.splice($scope.index_of, 1);
	}
	/* Submit the form and save the new owner */
	this.newOwner = function newOwner(){
		this.change();
		var req = {
			method: 'POST',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners",
			data: {
				"owner":{
					"first_name": $scope.new.first_name,
					"last_name": $scope.new.last_name,
					"country_id": $scope.new.country_id,
					"address": $scope.new.address,
					"phone_number": $scope.new.phone_number,
					"email": $scope.new.email,
					"owner_category_id": $scope.ownerCategoriesData.selected.id
				}
			}
		}			
		$http(req).then(function successCallback(response){
			alert("Propietario creado.");
			$scope.successMsg = true;
			angular.forEach($scope.new.plates, function(plate, index){
               var reqPlate = {
                   method: 'POST',
                   url: "http://"+ $scope.url + ":" + $scope.port + "/plates",
                   data: {
                       "plate": {
                           "owner_id": response.data.owner.id,
                           "plate_chars": plate
                       }
                   }
               }
               $http(reqPlate).then(function successCallback(response){
                   alert('Placa guardada.');
                   $scope.successMsg = true;
               }, function errorCallback(response){
                   alert('Error en guardar placa.');
                   $scope.errorMsg = true;
               });
           	});

		}, function errorCallback(response){
			alert("Error en guardar propietario.");
			$scope.errorMsg = true;
		});
	}

	// Check uniqueness
	this.checkUniqueness = function checkUniqueness(value){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners/search_by_country_id/" + value
		}).then(function successCallback(response){
			$scope.checkUniquenessInput = true;
		}, function errorCallback(response){
			$scope.checkUniquenessInput = false;
		});
	}
}
/*
* editOwnerCtrl
*/
function editOwnerCtrl($http, $scope, $stateParams, ownerCategories){
	$scope.checkUniquenessInput = false;
	$scope.invalidData = false;
	$scope.registeredPlate = false;
	$scope.plateInDb = false;
	$scope.newPlates = [];
	$scope.plans = "";
	var country_id = "";
	//function to find the plate in the user plates list
	function findInOwnerPlates(plate){
		var result = null;
		angular.forEach($scope.ownerData.plates, function(value, key){
			if(value.plate_chars === plate){
				result = true;
			}
		});

		return result;
	}

	this.invalidData = function invalidData(){
		$scope.invalidData = false;
		$scope.registeredPlate = false;
		$scope.plateInDb = false;
		$scope.successMsg = false;
		$scope.errorMsg = false;
	}

	$scope.$watchGroup(['editForm.$valid', 'editForm.$dirty', 'checkUniquenessInput'], function(value){

		if(value[0] && !value[2]){
			$('#step2Li').css('cursor','pointer');
			$('#step2Btn').css('pointer-events','auto');
		}
		if(!value[0] || value[2]){
			$('#step2Li').css('cursor','not-allowed');
			$('#step2Btn').css('pointer-events','none');
		}
	});

	/* Load category of owners */
	ownerCategories.async().then(function(response){
		$scope.ownerCategoriesData = response.data.collection;


		/* Load data for selected owner */
		var req = {
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners/" + $stateParams.id
		}
		$http(req).then(function successCallback(response){
			country_id = response.data.owner.country_id;
			$scope.ownerData = response.data.owner;
			$scope.ownerCategoriesData.selected = $scope.ownerCategoriesData[response.data.owner.owner_category.id - 1];
		}, function errorCallback(response){
			alert(response.message);
		});
	});

	// Add new plate to be saved
	this.addPlate = function addPlate(){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plates/check_uniqueness/plate_char",
			params: { 'plate[plate_chars]': $scope.ownerData.new_plate_chars.toUpperCase() }
		}).then(function successCallback(response){
			if($scope.newPlates.indexOf($scope.ownerData.new_plate_chars.toUpperCase()) >= 0){
				$scope.invalidData = true;
			}else if(findInOwnerPlates($scope.ownerData.new_plate_chars.toUpperCase())){
				$scope.registeredPlate = true;
			}else if(!response.data){
				$scope.plateInDb = true;
			}else{
				$scope.newPlates.push($scope.ownerData.new_plate_chars.toUpperCase());			
			}
			$('#plate_chars').val('');
			$scope.ownerData.new_plate_chars = '';
		}, function errorCallback(response){
			console.log("errorCallback: " + response.data);
		});
	}
	// remove table of list
	this.removePlate = function removePlate(index){
		$scope.newPlates.splice(index, 1);
	}
	/* submit function to update owner */
	this.updateOwner = function updateOwner(){
		this.invalidData();
		$scope.successMsg = false;
		var updateReq = {
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners/" + $stateParams.id,
			data: {
				"owner":{
					"first_name": $scope.ownerData.first_name,
					"last_name": $scope.ownerData.last_name,
					"country_id": $scope.ownerData.country_id,
					"address": $scope.ownerData.address,
					"phone_number": $scope.ownerData.phone_number,
					"email": $scope.ownerData.email,
					"position": $scope.ownerData.position,
					"owner_category_id": $scope.ownerCategoriesData.selected.id
				}
			}
		}

		$http(updateReq).then(function successCallback(response){
			$scope.successMsg = true;
			//alert("Propietario actualizado.");
			angular.forEach($scope.newPlates, function(plate, index){
               var reqPlate = {
                   method: 'POST',
                   url: "http://"+ $scope.url + ":" + $scope.port + "/plates",
                   data: {
                       "plate": {
                           "owner_id": response.data.owner.id,
                           "plate_chars": plate
                       }
                   }
               }
               $http(reqPlate).then(function successCallback(response){
                   //alert('Placa guardada.');
                   $scope.ownerData.plates.push(response.data.plate);
                   $scope.newPlates = [];
                   $scope.successMsg = true;
               }, function errorCallback(response){
                   $scope.errorMsg = true;
               });
           	});
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}

	// Check uniqueness
	this.checkUniqueness = function checkUniqueness(value){
		if(value != country_id){
			$http({
				method: 'GET',
				url: "http://"+ $scope.url + ":" + $scope.port + "/owners/search_by_country_id/" + value
			}).then(function successCallback(response){

				$scope.checkUniquenessInput = true;
			}, function errorCallback(response){
				$scope.checkUniquenessInput = false;
			});
		}else{
			$scope.checkUniquenessInput = false;
		}
	}
}
/* Attachments */
angular
    .module('inspinia')
    .controller('ownersMainCtrl', ownersMainCtrl)
    .controller('ownersNewCtrl', ownersNewCtrl)
    .controller('editOwnerCtrl', editOwnerCtrl)