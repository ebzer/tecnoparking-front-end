/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
    $httpProvider.defaults.useXDomain = true;

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: true
    });

    $urlRouterProvider.otherwise("/index/main");

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html"
        })
        .state('index.main', {
            url: "/main",
            templateUrl: "views/main.html",
            data: { pageTitle: 'Inicio' }
        })
        //Invoices
        .state('invoices', {
            abstract: true,
            url: "/invoices",
            templateUrl: "views/common/content.html"
        })
        .state('invoices.main', {
            url: '/invoices_main',
            templateUrl: 'views/invoices/invoices_main.html',
            data: { pageTitle: "Facturación" }
        })
        .state('invoices.new', {
            url: '/invoices_new',
            templateUrl: 'views/invoices/invoices_new.html',
            data: {  pageTitle: "Nuevo cobro" }
        })
        //Owners
        .state('owners', {
            abstract: true,
            url: '/owners',
            templateUrl: "views/common/content.html"
        })
        .state('owners.main', {
            url: "/owners_main",
            templateUrl: "views/owners/owners_main.html",
            data: { pageTitle: "Propietarios Registrados"}
        })
        //New Owner Wizard
        .state('owners.new', {
            url: "/owners_new",
            templateUrl: "views/owners/owners_new.html",
            data: { pageTitle: 'Nuevo propietario de vehículo'},
            resolve: {
                loadPlugin: function($ocLazyLoad){
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/steps/jquery.steps.css']
                        }
                    ]);
                }
            }
        })
        .state('owners.new.step_one', {
            url: '/step_one',
            templateUrl: 'views/owners/new_owner_wizard/step_one.html'
        })
        .state('owners.new.step_two', {
            url: '/step_two',
            templateUrl: 'views/owners/new_owner_wizard/step_two.html'
        })
        //Edit Owner Wizard
        .state('owners.edit', {
            url: "/owners_edit/:id", 
            templateUrl: "views/owners/owners_edit.html",
            controller: function($stateParams){
                $stateParams.id
            },
            data: { pageTitle: 'Edición de propietario registrado'},
            resolve: {
                loadPlugin: function($ocLazyLoad){
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/steps/jquery.steps.css']
                        }
                    ]);
                }
            }
        })
        .state('owners.edit.step_one', {
            url: '/step_one',
            templateUrl: 'views/owners/edit_owner_wizard/step_one.html'
        })
        .state('owners.edit.step_two', {
            url: '/step_two',
            templateUrl: 'views/owners/edit_owner_wizard/step_two.html'
        })
        //Owner Categories
        .state('owners.categories', {
            url: "/owner_categories_main",
            templateUrl: "views/owners/categories/categories_main.html",
            data: {pageTitle: 'Categorías de propietarios registrados.'}
        })
        .state('owners.new_category', {
            url: "/owner_new_category",
            templateUrl: "views/owners/categories/categories_new.html",
            data: {pageTitle: 'Nueva categoría de propietarios.'}
        })
        .state('owners.edit_category', {
            url: "/owner_edit_category/:id",
            templateUrl: "views/owners/categories/categories_edit.html",
            controller: function($stateParams){
                $stateParams.id
            },
            data: {pageTitle: 'Edición de categoría de propietarios.'}
        })
        // Plates
        .state('plates', {
            abstract: true,
            url: '/plates',
            templateUrl: "views/common/content.html"
        })
        .state('plates.main', {
            url: "/plates_main",
            templateUrl: "views/plates/plates_main.html",
            data: { pageTitle: "Placas Registradas" }
        })
        .state('plates.new', {
            url: "/plates_new",
            templateUrl: "views/plates/plates_new.html",
            data: { pageTitle: "Registro de nueva placa." }
        })
        .state('plates.edit', {
            url: "/plates_edit",
            templateUrl: "views/plates/plates_edit.html",
            data: { pageTitle: "Edición de placa registrada." }
        })
        // Plan Routes
        .state('plans', {
            abstract: true,
            url: '/plans',
            templateUrl: "views/common/content.html"
        })            
        .state('plans.main', {
            url: "/plans_main", 
            templateUrl: "views/plans/plans_main.html",
            data: { pageTitle: "Planes" }
        })
        .state('plans.new', {
            url: "/plans_new",
            templateUrl: "views/plans/plans_new.html", 
            data: { pageTitle: "Creación de nuevo plan"}
        })
        .state('plans.edit', {
            url: "/plans_edit/:id",
            templateUrl: "views/plans/plans_edit.html",
            controller: function($stateParams){
                $stateParams.id
            },
            data: { pageTitle: "Edición de plan"} 
        })
        // Users of the system
        .state('users', {
            abstract: true,
            url: "/users",
            templateUrl: "views/common/content.html",
        })
        .state('users.main', {
            url: "/users_main",
            templateUrl: "views/users/users_main.html",
            data: { pageTitle: 'Usuarios' }
        })
        .state('users.new', {
            url: "/users_new",
            templateUrl: "views/users/users_new.html",
            data: { pageTitle: 'Nuevo usuario' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css','js/plugins/iCheck/icheck.min.js']
                        }
                    ]);
                }
            }
        })
        .state('users.edit', {
            url: "/users_edit/:id", 
            templateUrl: "views/users/users_edit.html",
            controller: function($stateParams){
                $stateParams.id
            },
            data: { pageTitle: 'Edición de Usuario de Sistema'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        //Categories of users
        .state('users.categories', {
            url: "/users_categories_main",
            templateUrl: "views/users/categories/users_categories_main.html",
            data: {pageTitle: 'Categorías de usuarios de sistema.'}
        })
        .state('users.new_category', {
            url: "/users_categories_new",
            templateUrl: "views/users/categories/users_categories_new.html",
            data: {pageTitle: 'Nueva categoría de usuarios de sistema.'}
        })
        .state('users.edit_category', {
            url: "/users_categories_edit/:id",
            templateUrl: "views/users/categories/users_categories_edit.html",
            controller: function($stateParams){
                $stateParams.id
            },
            data: {pageTitle: 'Edición de categoría de usuarios de sistema.'}
        })
        //Configuraciones del sistema
        .state('configs', {
            abstract: true,
            url: "/configs",
            templateUrl: "views/common/content.html",
        })
        .state('configs.main', {
            url: "/configs_main",
            templateUrl: "views/configs/configs_main.html",
            data: { pageTitle: 'Configuraciones' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('configs.main_cameras', {
            url: "/cameras_main",
            templateUrl: "views/configs/main_cameras.html",
            data: { pageTitle: 'Cámaras' }
        })
        .state('configs.new_camera', {
            url: "/camera_new",
            templateUrl: "views/configs/new_camera.html",
            data: { pageTitle: 'Nueva cámara' }
        })
        .state('configs.edit_camera', {
            url: "/camera_edit/:id",
            templateUrl: "views/configs/edit_camera.html",
            controller: function($stateParams){
                $stateParams.id
            },
            data: { pageTitle: 'Edición de cámara'}
        })
        //Turnos y horarios
        .state('turns_and_schedules', {
            abstract: true,
            url: "/turns_and_schedules",
            templateUrl: "views/common/content.html"
        })
        .state('turns_and_schedules.main', {
            url: '/turns_and_schedules_main',
            templateUrl: "views/turns_and_schedules/main.html",
            data: { pageTitle: 'Turnos y Horarios' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([                        
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        }
                    ]);
                }
            }
        })
        .state('turns_and_schedules.turns', {
            url: '/turns',
            templateUrl: "views/turns_and_schedules/turns.html",
            data: { pageTitle: 'Turnos'},
            resolve: {
                loadPlugin: function($ocLazyLoad){
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css', 'js/plugins/clockpicker/clockpicker.js']
                        }
                    ]);
                }
            }
        })
        // Reportes
        .state('reports', {
            abstract: true,
            url: "/reports",
            templateUrl: "views/common/content.html"
        })
        .state('reports.main', {
            url: '/reports_main',
            templateUrl: "views/reports/index.html",
            data: { pageTitle: 'Reportes'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([                        
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        }
                    ]);
                }
            }
        })
        //Login
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html",
            data: { pageTitle: 'Login' }
        })
}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state, $window, $location) {
        $rootScope.$state = $state;
        $rootScope.$on( "$locationChangeStart", function(event, next, current) {
            if(!$window.sessionStorage.current_user){
                $location.path("/login");
            }else{
                if(next.split('#')[1] === "/login"){
                    $location.path(current.split('#')[1]);
                }
            }
        });
    });
