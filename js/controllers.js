/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */
function MainCtrl($scope, $http, $window, authInterceptor, $modal, $rootScope) { 
	$scope.url = location.host;
	$scope.port = "140";
	$rootScope.url = $scope.url;
	$rootScope.port = $scope.port;
	$scope.EMAIL_REGEX = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
	$scope.invalidData = true;
	$scope.user = [];
	$rootScope.barcode = {};
	$scope.spinner = false;
    console.log($scope.url);

	if($window.sessionStorage.current_user){
		$scope.current_user = JSON.parse($window.sessionStorage.current_user);		
	}
	// Login Handle
	this.login = function login(){
		var req = {
			method: 'POST',
			url: "http://"+ $scope.url + ":" + $scope.port + "/login",
			data: {
				"session":{
					"username": $scope.user.email,
					"password": $scope.user.password
				}
			}
		}
		$scope.spinner = true;
		$http(req).then(function successCallback(response){
			$window.sessionStorage.token = response.data.access_token;
			$window.sessionStorage.current_user_id = response.data.current_user_id;
			$window.sessionStorage.visited = true;
			$http({
				method: 'GET',
				url: "http://"+ $scope.url + ":" + $scope.port + "/users/" + $window.sessionStorage.current_user_id
			}).then(function successCallback(response){
				$window.sessionStorage.current_user = JSON.stringify(response.data.user);
				$scope.current_user = JSON.parse($window.sessionStorage.current_user);
				$window.location.href = '#/index/main';
			});
		}, function errorCallback(response){
			$window.sessionStorage.clear();
			$scope.invalidData = false;
			$scope.spinner = false;
		});
	}
	//Logout function
	this.logout = function logout(){
		$scope.spinner = false;
		var logoutReq = {
			method: 'DELETE',
			url: " http://"+ $scope.url + ":" + $scope.port + "/logout"
		}
		$http(logoutReq).then(function successCallback(response){
			$window.sessionStorage.clear();
			$window.location.href = '#/login';
		}, function errorCallback(response){
			
		});
	}

    // Get what lector reads
    var pressed = false; 
    var chars = [];
    $(window).keypress(function(e) {
        if (e.which >= 48 && e.which <= 57) {
            chars.push(String.fromCharCode(e.which));
        }
        if (pressed == false) {
            setTimeout(function(){
                if (chars.length == 10) {
					if($('div.modal').length == 0){
                    	var barcode = chars.join("");
                    	console.log("Barcode: " + barcode);
                    	searchBarcode(barcode);			        	
					}
                }
                chars = [];
                pressed = false;
            },300);
        }
        pressed = true;
    });

    function searchBarcode(barcode){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/barcodes/search_by_barcode/" + barcode
		}).then(function successCallback(response){
			if(response.data.barcode.barcodeable_type == "Entry"){
				if(!response.data.barcode.data.paid){
					$rootScope.barcode = response.data.barcode;
					$scope.open1('lg', true);
				}else{
					alert("Entrada pagada.");
				}
			}
			if(response.data.barcode.barcodeable_type == "Exit"){
				if(!response.data.barcode.data.expired || !response.data.barcode.data.used){
					var now = new Date();
					var created_at = new Date(response.data.barcode.data.created_at);
					var diff = Math.ceil((now - created_at)/(60*1000));
					var time_to_exit = created_at.getTime() + (20*60*1000);
					if(diff >= 20){
						if(response.data.barcode.data.owner){
							if(response.data.barcode.data.owner.owner_category_id == 1){
								alert("Ticket activo.");
							}
							if(response.data.barcode.data.owner.owner_category_id == 2){
								var end_validity_date = new Date(response.data.barcode.data.owner.validity.end_validity_date).getTime();
								if(time_to_exit < end_validity_date){
									alert("Ticket activo.");
								}else{
									console.log("Entró.")		
									$rootScope.barcode = response.data.barcode;
									$rootScope.barcode.data.created_at = new Date(time_to_exit);
									$scope.open1('lg', true);							
								}
							}
						}else{
							$rootScope.barcode = response.data.barcode;
							$rootScope.barcode.data.created_at = new Date(time_to_exit);
							$scope.open1('lg', true);
						}
					}else{
						alert("Ticket activo. Dispone de " + Math.ceil((time_to_exit - now)/(60*1000)) + " minutos para salir. =)");
					}
				}else{
					alert("Ticket usado.");
				}
			}
			if(response.data.barcode.barcodeable_type == "Invoice"){

			}
		}, function errorCallback(response){
			alert("Código no está en base de datos.");
		});
	}

	//Open Modal Window
    $scope.open1 = function (size, lector, barcode) {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal_example.html',
            size: size,
            controller: ModalInstanceCtrl,
            resolve: {
				barcode: function() {
			      	return $rootScope.barcode;
			    },
			    lector: function(){
			    	return lector;
			    }
			}
        });
    };
};


function ModalInstanceCtrl ($window, $scope, $modalInstance, $rootScope, barcode, lector, $http) {
	$scope.singleModel = 0;
	$scope.barcode_searched = false;
	$scope.lector = lector;
	$scope.generate_exit = false;
	$scope.exit_plate = false;

	var entry_date;
	var barcode = barcode;

	if(lector){
		display_barcode(barcode.barcode);
		entry_date = barcode.data.created_at;
		console.log("Entry date: ", entry_date);
		$scope.barcode_searched = true;
		$scope.entry_date = dateToString(barcode.data.created_at);
		if (barcode.barcodeable_type == "Exit") {			
			$scope.plate = barcode.data.plate;
			$scope.exit_plate = true;
		}
	}

	$scope.searchBarcode = function (barcode){
		search(barcode);		
	}

	$scope.$watch('plate', function(newValue, oldValue){
		if(newValue != null){
			$scope.plate = newValue.toUpperCase();
			if(newValue.length == 6){
				var regex = new RegExp("[A-Z]{3}[0-9]{3}");
				newValue = newValue.toUpperCase();
				if(regex.test(newValue)){
					$('#check_btn').removeAttr('disabled');
					check_plate(newValue);
				}else{
					$scope.amount_of_time = "";
					$('#check_btn').prop('disabled', true);
					$('#total_to_pay').css('display', 'none');
				}
			}else{
				$('#check_btn').prop('disabled', true);
				$('#total_to_pay').css('display', 'none');
				$scope.amount_of_time = "";
			}			
		}
	});

	$scope.ok = function () {
    	if($('#total_to_pay').css('display') == 'none'){
    		$http({
    			method: 'POST',
    			url: "http://"+ $scope.url + ":" + $scope.port + "/exits",
    			data: {
    				"barcode": barcode.barcode,
    				"plate": $scope.plate
    			}
    		}).then(function successCallback(response){
    			console.log("Exit generated." + response.data);
				$modalInstance.close();
    		}, function errorCallback(response){
    			console.log("Error generated." + response.data);
    			alert("Error.");
    		});
    	}
    	if($('#total_to_pay').css('display') == 'block'){
    		$http({
	    		method: 'POST',
	    		url: "http://"+ $scope.url + ":" + $scope.port + "/invoices",
	    		data: {
	    			"barcode": barcode.barcode,
	    			"plate": $scope.plate,
	    			"current_user_id": $window.sessionStorage.current_user_id,
	    			"type_of": "Entry"
	    		}
	    	}).then(function successCallback(response){
	    		console.log("Success Response: Invoice = " + response.data.invoice.id);
	    		$modalInstance.close();
	    	}, function errorCallback(response){
	    		console.log("Error Response: " + response);
	    		alert("Error.");
	    	});
	    }

    };

	function check_plate(plate_chars){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plates/search_by_plate/" + plate_chars
		}).then(function successCallback(response){
			if(response.data.plate.owner){
	            if(response.data.plate.owner.owner_category_id == 1){
	            	$scope.amount_of_time = amount_of_time(barcode.data.created_at)[0];
	            }
	            if(response.data.plate.owner.owner_category_id == 2){
	            	if(response.data.plate.owner.owner_active && (new Date().getTime() < new Date(response.data.plate.owner.owner_validity.end_validity_date)) ){	            		
	            		check_validity_date(response.data.plate.owner.owner_validity.start_validity_date, response.data.plate.owner.owner_validity.end_validity_date);	            		
	            	}else{
	            		alert("Propietario no activo.");
	            		$scope.amount_of_time = amount_of_time(barcode.data.created_at)[0];
	            		$scope.total_to_pay = amount_of_time(barcode.data.created_at)[1];
            			$('#total_to_pay').css('display', 'block');
	            	}
	            }
			}else{
            	$scope.amount_of_time = amount_of_time(barcode.data.created_at)[0];
            	$scope.total_to_pay = amount_of_time(barcode.data.created_at)[1];
            	$scope.generate_exit = false;
            	$('#total_to_pay').css('display', 'block');
        	}

		}, function errorCallback(response){
			console.log("Error: ", response.data.message +"\n");
			$scope.amount_of_time = amount_of_time(barcode.data.created_at)[0];
        	$scope.total_to_pay = amount_of_time(barcode.data.created_at)[1];
        	$scope.generate_exit = false;
        	$('#total_to_pay').css('display', 'block');
		});
	}

	function check_validity_date(start_validity_date, end_validity_date){
		var oneDay = 24*60*60*1000;
		var oneMinute = 60*1000;

		var new_entry_date 				= new Date(entry_date);
		var new_exit_date 				= new Date();
		var new_start_validity_date 	= new Date(start_validity_date);
		var new_end_validity_date 		= new Date(end_validity_date);

		if(new_entry_date.getTime() >= new_start_validity_date.getTime()){
			if(new_exit_date.getTime() <= new_end_validity_date.getTime()){

				$scope.generate_exit = true;
				$scope.amount_of_time = amount_of_time(barcode.data.created_at)[0];
			}else{
				$scope.entry_date = dateToString(new_end_validity_date);
				$scope.amount_of_time = amount_of_time(new_end_validity_date)[0];
				$scope.total_to_pay = amount_of_time(new_end_validity_date)[1];
				$scope.generate_exit = false;
				$('#total_to_pay').css('display', 'block');
			}
		}else{
			if(new_exit_date.getTime() <= new_end_validity_date.getTime()){
				$scope.amount_of_time = amount_of_time(entry_date, new_start_validity_date)[0];
				$scope.total_to_pay = amount_of_time(entry_date, new_start_validity_date)[1];
				$scope.generate_exit = false;
				$('#total_to_pay').css('display', 'block');
			}else{
				console.log("Salida mayor a new_end_validity_date");
				console.log("Generate two entries");
			}
		}
		if (new_entry_date.getTime() > new_end_validity_date.getTime()){
			$scope.amount_of_time = amount_of_time(barcode.data.created_at)[0];
    		$scope.total_to_pay = amount_of_time(barcode.data.created_at)[1];
    		$scope.generate_exit = false;
			$('#total_to_pay').css('display', 'block');
		}

	}

	function display_barcode(barcode){
		setTimeout(function(){
			$("#barcode_img").JsBarcode(
				barcode,
				{format:"ITF", 
				displayValue:true, 
				fontSize:20
			});
		}, 10);
	}

	function search(barcode){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/barcodes/search_by_barcode/" + barcode
		}).then(function successCallback(response){
			if(response.data.barcode.barcodeable_type == "Entry"){
				$scope.exit_plate = false;
				if(!response.data.barcode.data.paid){
					barcode = response.data.barcode;
					entry_date = response.data.barcode.data.created_at;
					$scope.entry_date = dateToString(response.data.barcode.data.created_at);
					$scope.plate = response.data.barcode.data.plate;
					display_barcode(response.data.barcode.barcode);
					$scope.barcode_searched = true;
				}else{
					alert("Entrada pagada.");
				}
			}
			if(response.data.barcode.barcodeable_type == "Exit"){
				$scope.plate = barcode.data.plate;
				$scope.exit_plate = true;
			}
			if(response.data.barcode.barcodeable_type == "Invoice"){

			}
		}, function errorCallback(response){
			$scope.barcode_searched = false;
			alert("Código no está en base de datos.");
		});
	}

	function dateToString(date){
		days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
		months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

		var date = new Date(date);
		
		function addZero(value){
			if (value < 10) {
				return "0" + value;
			}
			return value;
		}

		function toHours(value){
			if (value > 12) {
				return value - 12;
			}
			if (value == 0) {
				return 12;
			}
			return value;
		}

		function AMPM(value){
			if(value >= 0 && value <= 11){
				return " A.M.";
			}
			if(value >= 12 && value <= 23 ){
				return " P.M.";
			}
		}

		return(days[date.getDay()] + ", " + date.getDate() + " de " + months[date.getMonth()] + " de " + date.getFullYear() + " - " + addZero(toHours(date.getHours())) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds()) + AMPM(date.getHours()) );
	}

	function amount_of_time(entry, exit){
		if (exit) {
			var n = new Date(exit);			
		}else{
			var n = new Date();
		}

		var miliseconds = Date.parse(n) - Date.parse(entry);

		var parked_time_seconds = miliseconds/1000;
		var parked_time_minutes = parked_time_seconds/60;
		var parked_time_hours   = parked_time_minutes/60;

		var amount_of_time = Math.floor(parked_time_hours) + " hora(s) " +
		Math.ceil((parked_time_hours%1) * 60) + " minuto(s)";

		var total_to_pay = Math.ceil(parked_time_hours) * 2000;

		return [amount_of_time, total_to_pay];
	}

	$("#new_charge").keypress(function(e){
	    if ( e.which === 13 ) {
	        e.preventDefault();
	    }
	});

};

angular
    .module('inspinia')
    .controller('MainCtrl', MainCtrl)