function mainReportsCtrl($scope, $http){
	$scope.report = {};
	$scope.report.dates = {startDate: new Date(), endDate: new Date()};
	$scope.turns = [];
	$scope.users = [];


	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ':' + $scope.port + '/turns',
	}).then(function successCallback(response){
		$scope.turns.push({
			'id': 0,
			'name': "Turnos"
		});
		angular.forEach(response.data.turns, function(value, key) {
		  $scope.turns.push({
		  	'id': value.id,
		  	'name': "Turno " + (key + 1) + ": " + value.initial_time + " - " + value.final_time
		  });
		});	
	});

	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ':' + $scope.port + '/users',
	}).then(function successCallback(response){
		$scope.users.push({
			'id': 0,
			'name': "Usuarios"
		});
		angular.forEach(response.data.collection, function(value, key) {
		  $scope.users.push({
		  	'id': value.id,
		  	'name': value.first_name + " " + value.last_name
		  });
		});
	});	

	this.generate_report = function generate_report(){
		console.log($scope.report);
	}
}

angular
	.module('inspinia')
	.controller('mainReportsCtrl', mainReportsCtrl)