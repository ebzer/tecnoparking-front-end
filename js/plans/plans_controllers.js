/* 
* Plans Main Page Controller *
*/
function plansMainCtrl($http, $scope){

	$scope.plans = null;

	$http({
		method: 'GET',
		url: "http://"+ $scope.url + ":" + $scope.port + "/plans"			
	}).then(function successCallback(response){
		$scope.plans = response.data.plans;
	});


	this.activate = function activate(row, id_plan, active){
		$http({
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plans/" + id_plan,
			data: {
				"plan":
				{
					"active": !active
				}
			}
		}).then(function successCallback(response){
			$scope.plans[row].active = response.data.plan.active;
		}, function errorCallback(response){
			alert("Error en cambiar en la activación.");
		});
	}

}
/*
* New Plan Page Controller *
*/
function newPlanCtrl($http, $scope){
	$scope.successMsg = false;
	$scope.errorMsg = false;
	$scope.plan = {
		name: '',
		plan_type_by_days: true,
		quantity: '',
		price: ''
	}

	this.newPlan = function newPlan(plan){
		$http({
			method: 'POST',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plans",
			data: { plan }
		}).then(function successCallback(response){
			$scope.successMsg = true;
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}
}
/* 
* Edit Plan Page Controller*
*/
function editPlanCtrl($http, $scope, $stateParams){
	$scope.plan = null;
	$scope.successMsg = false;
	$scope.errorMsg = false;
	$http({
		method: 'GET',
		url: "http://"+ $scope.url + ":" + $scope.port + "/plans/" + $stateParams.id
	}).then(function successCallback(response){
		$scope.plan = response.data.plan;
		$scope.plan.quantity = parseInt(response.data.plan.quantity);
		$scope.plan.price = parseInt(response.data.plan.price);
	});

	this.updatePlan = function updatePlan(plan){
		$http({
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plans/" + $stateParams.id,
			data: { plan }
		}).then(function successCallback(response){
			$scope.successMsg = true;
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}
}

angular
    .module('inspinia')
    
    .controller('plansMainCtrl', plansMainCtrl)
    .controller('newPlanCtrl', newPlanCtrl)
    .controller('editPlanCtrl', editPlanCtrl)