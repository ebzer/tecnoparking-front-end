// Users Controllers 

function usersMainCtrl($scope, $window, $http) {
	$scope.usersCollection = '';
	var req = {
		method: 'GET',
		url: "http://"+ $scope.url + ":" + $scope.port + "/users"
	}
	
	$http(req).then(function successCallback(response){
		$scope.usersCollection = response.data.collection;
	});
	

	this.blockUser = function blockUser(row, idUser, active){
		var blockReq = {
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/users/" + idUser,
			data: {
				"user":{
					"active": !active
				}
			}
		}
		$http(blockReq).then(function successCallback(response){
			$scope.usersCollection[row].active = response.data.user.active;
		}, function errorCallback(response){
			alert("Error");
		});
	}
};

/**
* New User Controller - Send data to api to create new user in database.
**/
function usersNewCtrl($scope, $window, $http, userCategories, SweetAlert){
	$scope.password = "";
	$scope.confirmation = "";
	$scope.userCategoriesData = "";
	$scope.switch1 = false;
	$scope.switch2 = false;
	$scope.switch3 = false;
	$scope.switch4 = false;
	/* Load category of users */
	userCategories.async().then(function(response){
		$scope.userCategoriesData = response.data.collection;
		$scope.userCategoriesData.selected = $scope.userCategoriesData[0];
	});

	/* Submit the form and save the new user */
	this.newUser = function newUser(){
		var PASS_REGEX = /^[a-zA-Z0-9]*$/; //to check if the input is empty or has white spaces.
		if($scope.password != $scope.confirmation){
			SweetAlert.swal({
	            title: "Contraseñas no coinciden.",
	            type: "warning",
	            confirmButtonColor: "#f8ac59"
	        });
		}
		else{
			if(PASS_REGEX.test($scope.password)){
				var req = {
					method: 'POST',
					url: "http://"+ $scope.url + ":" + $scope.port + "/users",
					data: {
						"user":{
							"first_name": $scope.new.first_name,
							"last_name": $scope.new.last_name,
							"country_id": $scope.new.country_id,
							"email": $scope.new.email,
							"password": $scope.password,
							"user_category_id": $scope.userCategoriesData.selected.id,
							"active": true
						}
					}
				}			
				$http(req).then(function successCallback(response){
					$scope.successMsg = true;
					$scope.errorMsg = false;
					SweetAlert.swal({
			            title: "¡Buen trabajo!",
			            text: "Usuario guardado exitosamente.",
			            type: "success",
			            confirmButtonColor: "#1c84c6"
			        });
				}, function errorCallback(response){
					SweetAlert.swal({
			            title: "¡Error!",
			            text: "Usuario no pudo ser guardado.",
			            type: "error",
			            confirmButtonColor: "#ed5565"
			        });
					$scope.successMsg = false;
					$scope.errorMsg = true;
				});
			}
			else
			{
				SweetAlert.swal({
		            title: "Contraseña debe contener solo letras y números.",
		            type: "warning",
		            confirmButtonColor: "#f8ac59"
		        });
			}
		}
	}


	$scope.$watchGroup(['switch1', 'switch2', 'switch3', 'switch4'], function(value){
		if(value[0]){
			$('input#switch2').checked = true;
		}
	});
};

/**
* Edit User Controller - Request data of a user 
**/
function usersEditCtrl($scope, $http, $stateParams, userCategories, SweetAlert){
	$scope.password = "";
	$scope.confirmation = "";
	$scope.userCategoriesData = "";
	/* Load category of users */
	userCategories.async().then(function(response){
		$scope.userCategoriesData = response.data.collection;
		/* Load data for selected user */
		var req = {
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/users/" + $stateParams.id
		}
		$http(req).then(function successCallback(response){
			$scope.userData = response.data.user;
			$scope.userData.selected = $scope.userCategoriesData[response.data.user.user_category.category_id - 1];
		}, function errorCallback(response){
			console.log(response);
			alert(response.status);
		});
	});

	/* submit function to update user */

	this.updateUser = function updateUser(){
		$scope.successMsg = false;
		var PASS_REGEX = /^[a-zA-Z0-9]*$/; //to check if the input is empty or has white spaces.
		if($scope.password != $scope.confirmation){
			SweetAlert.swal({
	            title: "Contraseñas no coinciden.",
	            type: "warning",
	            confirmButtonColor: "#f8ac59"
	        });
		}
		else{
			var updateReq = {
				method: 'PUT',
				url: "http://"+ $scope.url + ":" + $scope.port + "/users/" + $stateParams.id,
				data: {
					"user":{
						"nombres": $scope.userData.nombres,
						"apellidos": $scope.userData.apellidos,
						"country_id": $scope.userData.country_id,
						"email": $scope.userData.email,
						"user_category_id": $scope.userData.selected.id
					}
				}
			}
			if($scope.password){
				updateReq.data.user["password"] = $scope.password;
			}
			if(PASS_REGEX.test($scope.password)){
				$http(updateReq).then(function successCallback(response){
					$scope.successMsg = true;
					SweetAlert.swal({
			            title: "¡Buen trabajo!",
			            text: "Usuario guardado exitosamente.",
			            type: "success",
			            confirmButtonColor: "#1c84c6"
			        });
				}, function errorCallback(response){
					$scope.errorMsg = true;
					SweetAlert.swal({
			            title: "¡Error!",
			            text: "Usuario no pudo ser guardado.",
			            type: "error",
			            confirmButtonColor: "#ed5565"
			        });
				});				
			}else{
				SweetAlert.swal({
		            title: "Contraseña debe contener solo letras y números.",
		            type: "warning",
		            confirmButtonColor: "#f8ac59"
		        });
			}
		}
	}
}

angular
    .module('inspinia')
    .controller('usersMainCtrl', usersMainCtrl)
    .controller('usersNewCtrl', usersNewCtrl)
    .controller('usersEditCtrl', usersEditCtrl)