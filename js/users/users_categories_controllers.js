/*
* usersCategoriesMainCtrl Controller
*/
function usersCategoriesMainCtrl($http, $scope){
	var req = {
		method: 'GET', 
		url: "http://"+ $scope.url + ":" + $scope.port + "/user_categories"
	}
	$http(req).then(function successCallback(response){
		$scope.categoriesCollection = response.data.collection;
	});
}
/*
* usersCategoriesNewCtrl Controller
*/
function usersCategoriesNewCtrl($http, $scope){
	$scope.successMsg = false;
	$scope.errorMsg = false;
	this.newCategory = function newCategory(){
		var req = {
			method: 'POST',
			url: "http://"+ $scope.url + ":" + $scope.port + "/user_categories",
			data: {
				"user_category":{
					"nombre": $scope.category_name,
					"descripcion": $scope.category_description
				}
			}
		}
		$http(req).then(function successCallback(response){
			$scope.successMsg = true;
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}
}
/*
* usersCategoriesEditCtrl Controller
*/
function usersCategoriesEditCtrl($http, $scope, $stateParams){
	$scope.successMsg = false;
	$scope.errorMsg = false;
	var req = {
		method: 'GET', 
		url: "http://"+ $scope.url + ":" + $scope.port + "/user_categories/" + $stateParams.id
	}
	$http(req).then(function successCallback(response){
		$scope.category_name = response.data.user_category.nombre;
		$scope.category_description = response.data.user_category.descripcion;
	});
	this.editCategory = function editCategory(){
		var req = {
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/user_categories/" + $stateParams.id,
			data: {
				"user_category":{
					"nombre": $scope.category_name,
					"descripcion": $scope.category_description
				}
			}
		}
		$http(req).then(function successCallback(response){
			$scope.successMsg = true;
		}, function errorCallback(response){
			$scope.errorMsg = true;
		});
	}
}

/* Attachments */
angular
    .module('inspinia')
    .controller('usersCategoriesMainCtrl', usersCategoriesMainCtrl)
    .controller('usersCategoriesNewCtrl', usersCategoriesNewCtrl)
    .controller('usersCategoriesEditCtrl', usersCategoriesEditCtrl)