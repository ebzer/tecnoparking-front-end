/*
* platesMainCtrl Controller
*/
function platesMainCtrl($http, $scope, ownerCategories){
	$scope.platesCollection = {};
	$http({
		method: 'GET',
		url: "http://"+ $scope.url + ":" + $scope.port + "/plates"
	}).then(function successCallback(response){
		$scope.platesCollection = response.data.collection;
	});

	this.blockPlate = function blockPlate(index, id_plate, active)
	{
		$http({
			method: 'PUT',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plates/" + id_plate,
			data: {
				"plate":
				{
					"active": !active
				}
			}
		}).then(function successCallback(response){
			$scope.platesCollection[index].active = response.data.plate.active;
		}, function errorCallback(response){
			alert("Error en bloqueo.");
		});
	}

}
/*
* platesNewCtrl Controller
*/
function platesNewCtrl($http, $scope, ownerCategories){
	$scope.disabledSubmit = true;
	$scope.invalidData = false;
	$scope.registeredPlate = false;
	$scope.plateInDb = false;
	$scope.plates = [];

	this.invalidData = function invalidData(){
		$scope.invalidData = false;
		$scope.registeredPlate = false;
		$scope.plateInDb = false;
	}

	$scope.$watch('plates.length', function(value){
		if(value > 0){
			$scope.disabledSubmit = false;
		}
		if(value == 0){
			$scope.disabledSubmit = true;
		}
	});

	//function to find the plate in the user plates list
	function findInOwnerPlates(plate){
		var result = null;
		angular.forEach($scope.ownerData.plates, function(value, key){
			if(value.plate_chars === plate){
				result = true;
			}
		});

		return result;
	}

	/* Load category of owners */
	ownerCategories.async().then(function(response){
		$scope.ownerCategoriesData = response.data.collection;
	});
	
	this.findOwner = function findOwner(value){
		$scope.plates = [];
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/owners/search_by_country_id/" + value
		}).then(function successCallback(response){
			$scope.ownerData = response.data.owner;
		}, function errorCallback(response){
			$scope.ownerData = {}
			alert("No se encontró propietario con cédula " + value + ".");
		});
	}
	/* Add plates to table and json data */
	this.addPlate = function addPlate(plate){
		$http({
			method: 'GET',
			url: "http://"+ $scope.url + ":" + $scope.port + "/plates/check_uniqueness/plate_char",
			params: { 'plate':{
				'plate_chars': plate }
			}
		}).then(function successCallback(response){
			$scope.index_of = $scope.plates.indexOf(plate.toUpperCase());
			if($scope.index_of >= 0){
				$scope.invalidData = true;
			}else if(findInOwnerPlates(plate.toUpperCase())){
				$scope.registeredPlate = true;
			}else if(!findInDb(plate.toUpperCase())){
				$scope.plateInDb = true;
			}else{
				$scope.plates.push(plate.toUpperCase());			
			}
			$('#plate_chars').val('');
			$scope.ownerData.plate_chars = "";
		}, function errorCallback(response){
			console.log(response.data);
		});
	}
	/* Remove plate from collection */
	this.removePlate = function removePlate(index){
		$scope.plates.splice(index, 1);
	}
	// Save plates to DB
	this.newPlate = function newPlate(){
		this.invalidData();
			angular.forEach($scope.plates, function(plate, index){
               var reqPlate = {
                   method: 'POST',
                   url: "http://"+ $scope.url + ":" + $scope.port + "/plates",
                   data: {
                       "plate": {
                           "owner_id": $scope.ownerData.id,
                           "plate_chars": plate
                       }
                   }
               }
               $http(reqPlate).then(function successCallback(response){
                   //alert('Placa guardada.');
                   $scope.successMsg = true;
                   $scope.ownerData = response.data.owner;
               }, function errorCallback(response){
                   $scope.errorMsg = true;
               });
        	});
	}

}
/* Attachments */
angular
    .module('inspinia')

    .controller('platesMainCtrl', platesMainCtrl)
    .controller('platesNewCtrl', platesNewCtrl)