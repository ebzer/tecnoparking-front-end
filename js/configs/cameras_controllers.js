// Cameras Main Page
function camerasMainCtrl($http, $scope){
	$scope.cameras_refs;
	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ":" + $scope.port + "/camera_refs"
	}).then(function successCallback(response){
		$scope.cameras_refs = response.data.cameras;
	});
}

// New Camera Page
function newCameraCtrl($http, $scope){
	$scope.brands = [];

	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ":" + $scope.port + "/camera_brands"
	}).then(function successCallback(response){
		angular.forEach(response.data.brands, function(value, key) {
		  $scope.brands.push(value.brand);
		});
	});

	this.save_camera = function save_camera(){
		$http({
			method: 'POST',
			url: 'http://' + $scope.url + ":" + $scope.port + "/camera_refs",
			data: {
				'camera_ref': {
					'brand': $scope.camera.brand,
					'reference': $scope.camera.reference,
					'path': $scope.camera.path
				}
			}
		}).then(function successCallback(response){
			console.log(response.data);
			alert('Cámara creada exitosamente.')
		}, function errorCallback(response){
			console.log("Error: " + response);
		});
	}
}

angular
	.module('inspinia')
	.controller('camerasMainCtrl', camerasMainCtrl)
	.controller('newCameraCtrl', newCameraCtrl)