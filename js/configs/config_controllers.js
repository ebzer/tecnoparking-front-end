// Config Main page

function configMainCtrl($http, $scope, notify){
	$scope.parking_setting;
	$scope.parking_entries = [];
	$scope.parking_exits = [];
	$scope.cameras_refs;

	$http({
		method: 'GET',
		url: "http://" + $scope.url + ":" + $scope.port + "/parking_setting"
	}).then(function successCallback(response){
		$scope.parking_setting = response.data.parking_setting;
		accesses();
	});


	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ":" + $scope.port + "/camera_refs"
	}).then(function successCallback(response){
		$scope.cameras_refs = response.data.cameras;
	});


	function accesses(){
		$scope.parking_entries = [];
		$scope.parking_exits = [];
		$http({
			method: 'GET',
			url: 'http://' + $scope.url + ":" + $scope.port + "/parking_setting/accesses"
		}).then(function successCallback(response){
			$scope.parking_accesses = response.data.parking_setting;
			angular.forEach(response.data.parking_setting, function(value, key){
				if (value.type_of == true && value.deleted == false) {
					if(!value.description){
						value.description = "Entrada no. " + ($scope.parking_entries.length + 1);						
					}
					$scope.parking_entries.push(value);
				}
				if (value.type_of == false && value.deleted == false) {
					if(!value.description){
						value.description = "Salida no. " + ($scope.parking_exits.length + 1);						
					}
					$scope.parking_exits.push(value);
				}
			});
		});
	}

	this.save_setting = function save_setting(settings){
		$http({
			method: 'POST',
			url: "http://" + $scope.url + ":" + $scope.port + "/parking_setting",
			data: {
				"parking_setting": settings
			}
		}).then(function successCallback(response){
			alert("Configuración guardada exitosamente.");
			accesses();
		}, function errorCallback(response){
			console.log(response.data);
		});
	}

	this.save_access = function save_access(access){
		access.camera_ref_id = parseInt(access.camera_ref_id);
		$http({
			method: 'POST',
			url: "http://" + $scope.url + ":" + $scope.port + "/parking_setting/save_access",
			data: {
				"access": access
			}
		}).then(function successCallback(response){			
			console.log(response.data);
			notify({
			    message:'Cámara configurada.',
			    classes: 'alert-info',
			    templateUrl: 'views/common/notify.html'
			}); 
		}, function errorCallback(response){
			console.log(response.data);
			console.log("Error al guardar.");
		});
	}
}

angular
	.module('inspinia')
	.controller('configMainCtrl', configMainCtrl)