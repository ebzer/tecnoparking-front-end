// turnsAndSchedulesMainCtrl
function turnsAndSchedulesMainCtrl($http, $scope, $rootScope){
	$rootScope.turns_count;
	$rootScope.turns = [];
	$rootScope.users = [];

	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ':' + $scope.port + '/users',
	}).then(function successCallback(response){
		$rootScope.users = response.data.collection;
	});

	get_turns();

	function get_turns(){
		$http({
			method: 'GET',
			url: "http://" + $scope.url + ":" + $scope.port + "/turns"
		}).then(function successCallback(response){
			$rootScope.turns_count = response.data.turns.length;
			$rootScope.turns = response.data.turns;
		});		
	}

	this.set_turns = function set_turns(quantity){
		$http({
			method: 'POST',
			url: 'http://' + $scope.url + ':' + $scope.port + '/turns/set_turns',
			data: { 'turn': { 'turns_quantity': quantity }}
		}).then(function successCallback(response){
			console.log(response.data);
			get_turns();
		});			
	}

	this.save_turn_hours = function save_turn_hours(turn){
		$http({
			method: 'PUT',
			url: 'http://' + $scope.url + ':' + $scope.port + '/turns/' + turn.id,
			data: { 'turn': turn }
		}).then(function successCallback(response){
			console.log(response.data);
		}, function errorCallback(response){
			console.log(response.data);
		});
	}
}

function CalendarCtrl($scope, $modal, $http, $rootScope) {
	$scope.the_date = new Date();
	$rootScope.today = new Date();
	console.log($rootScope.today);
	$scope.calendar_title = "";
	$rootScope.times = [];
	generate_calendar($rootScope.today);

	function generate_calendar(){
		$rootScope.times = [];
		var days_of_month = get_days_of_month($rootScope.today.getFullYear(), $rootScope.today.getMonth());
		var first_day = get_first_day_of_month($rootScope.today);
		var day_numbers = [];
		$scope.schedules = [];

		if(first_day > 0){
			var days_ago = $rootScope.today;
			days_ago.setDate(-(first_day-1));

			for(var i = 1; i <= first_day; i++){
				day_numbers.push([days_ago.getFullYear(), days_ago.getMonth(), days_ago.getDate()]);
				days_ago.setDate(days_ago.getDate() + 1);
			}
		}

		for(var i = 1; i <= days_of_month; i++){
			day_numbers.push([$rootScope.today.getFullYear(), $rootScope.today.getMonth(), i]);
		}

		var rest = 42 - day_numbers.length;
		for(var i = 1; i <= rest; i++){
			day_numbers.push([$rootScope.today.getFullYear(), $rootScope.today.getMonth() + 1, i]);
		}

		for(var i = 0; i <= 5; i++){
			var days_of_week = [];
			for(var j = 1; j <= 7; j++){
				days_of_week.push({
					index: j+(7*i),
					number: day_numbers[j+(7*i)-1],
					events: []
				});
			}
			$rootScope.times.push(days_of_week);

			if(i == 5){
				var start_date = $rootScope.times[0][0].number[0] + "-" + ($rootScope.times[0][0].number[1] + 1) + "-" + $rootScope.times[0][0].number[2];
				var end_date   = $rootScope.times[5][6].number[0] + "-" + ($rootScope.times[5][6].number[1] + 1) + "-" + $rootScope.times[5][6].number[2];
				$http({
					method: 'POST',
					url: 'http://' + $scope.url + ':' + $scope.port + '/schedules/schedules_by_month',
					data: {
						schedule: {
							start_date: start_date,
							end_date: end_date							
						}
					}
				}).then(function successCallback(response){
					for(var i = 0; i <= 5; i++){
						for(var j = 0; j <= 6; j++){
							var current_date = new Date($rootScope.times[i][j].number[0], $rootScope.times[i][j].number[1], $rootScope.times[i][j].number[2])							
							$rootScope.times[i][j].events = [];
							for(var x = 0; x < response.data.schedules.length; x++){
								var operation_date = new Date(response.data.schedules[x].operation_date);
								operation_date.setDate(operation_date.getDate()+1);					
								if(operation_date.getFullYear() == current_date.getFullYear() && operation_date.getMonth() == current_date.getMonth() && operation_date.getDate() == current_date.getDate()){									
									$rootScope.times[i][j].events.push(response.data.schedules[x].turn.initial_time + ": " + response.data.schedules[x].user);
								}
							}
						}
					}	
				});
			}
		}

		$scope.calendar_title = getMonthYear($rootScope.today.getMonth(), $rootScope.today.getFullYear());
	}

	function get_days_of_month(year, month){
		var date = new Date(year, month+1, 0);
		return date.getDate();
	}

	function get_first_day_of_month(date){
		date.setDate(1);
		return date.getDay();
	}


    function getMonthYear(month, year){
    	months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    	return months[month] + " " + year;
    }

    function get_name_of_day(day){
    	days = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
    	return days[day];
    }

	this.next_month = function next_month(){
		$rootScope.today.setMonth($rootScope.today.getMonth() + 1);
		generate_calendar($rootScope.today);
	}

	this.prev_month = function prev_month(){
		$rootScope.today.setMonth($rootScope.today.getMonth() - 1);
		generate_calendar($rootScope.today);
	}

    this.new_schedule = function new_schedule(){
    	var modalInstanceTurnSchedule = $modal.open({
            templateUrl: 'views/turns_and_schedules/new_schedule_form.html',
            controller: ModalInstanceTurnScheduleCtrl,
            size: 'lg'
        });
    }
}

function ModalInstanceTurnScheduleCtrl ($scope, $modalInstance, $rootScope, $http) {
	$scope.schedule = {};
	date = new Date();
	date.setHours(0,0,0,0);
	$scope.schedule.operation_date = {startDate: date, endDate: date};	
	$rootScope.turns_count;
	$rootScope.turns = [];
	$rootScope.users = [];

	$http({
		method: 'GET',
		url: 'http://' + $scope.url + ':' + $scope.port + '/users',
	}).then(function successCallback(response){
		$rootScope.users = response.data.collection;
	});

	get_turns();

	function get_turns(){
		$http({
			method: 'GET',
			url: "http://" + $scope.url + ":" + $scope.port + "/turns"
		}).then(function successCallback(response){
			$rootScope.turns_count = response.data.turns.length;
			$rootScope.turns = response.data.turns;
		});		
	}
    $scope.ok = function (schedule) {
        $http({
        	method: 'POST',
        	url: 'http://' + $scope.url + ':' + $scope.port + '/schedules',
        	data: { 'schedule': $scope.schedule }
        }).then(function successCallback(response){        	
        	for(var i = 0; i <= 5; i++){
				for(var j = 0; j <= 6; j++){
					var current_date = new Date($rootScope.times[i][j].number[0], $rootScope.times[i][j].number[1], $rootScope.times[i][j].number[2])							
					for(var x = 0; x < response.data.schedules.length; x++){
						var operation_date = new Date(response.data.schedules[x].operation_date);
						operation_date.setDate(operation_date.getDate()+1);					
						if(operation_date.getFullYear() == current_date.getFullYear() && operation_date.getMonth() == current_date.getMonth() && operation_date.getDate() == current_date.getDate()){									
							if($rootScope.times[i][j].events.length == 3){
								$rootScope.times[i][j].events.length = [];
							}
							$rootScope.times[i][j].events.push(response.data.schedules[x].turn.initial_time + ": " + response.data.schedules[x].user);
						}
					}
				}
			}
        });
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

angular
	.module('inspinia')
	.controller('turnsAndSchedulesMainCtrl', turnsAndSchedulesMainCtrl)
	.controller('CalendarCtrl', CalendarCtrl)