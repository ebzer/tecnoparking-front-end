/*
* entriesMain Controller
*/
function entriesCtrl($scope, $http) 
{
	$scope.entries = {};

	$http({
		method: 'GET',
		url: "http://" + $scope.url + ":" + $scope.port + "/entries"
	}).then(function successCallback(response){
		$scope.entries = response.data.entries;
		angular.forEach($scope.entries, function(value, key){
			value.photo_url = "http://" + $scope.url + ":" + $scope.port + value.photo_url
			value.segment_url = "http://" + $scope.url + ":" + $scope.port + value.photo_url
			value.created_at = formatDate(value.created_at);
		});
	});

	function formatDate(date){
		days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
		months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		var date = new Date(date);
		function addZero(value){
			if (value < 10) {
				return "0" + value;
			}
			return value;
		}

		function toHours(value){
			if (value > 12) {
				return value - 12;
			}
			if (value == 0) {
				return 12;
			}
			return value;
		}

		function AMPM(value){
			if(value >= 0 && value <= 11){
				return " A.M.";
			}
			if(value >= 12 && value <= 23 ){
				return " P.M.";
			}
		}

		return(days[date.getDay()] + ", " + date.getDate() + " de " + months[date.getMonth()] + " de " + date.getFullYear() + " - " + addZero(toHours(date.getHours())) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds()) + AMPM(date.getHours()) );
	}

	$('#plate1').click(function(){
		alert("Click!");
	});
}

angular
	.module('inspinia')
	.controller('entriesCtrl', entriesCtrl)