/* Interceptor to add current user token to http requests */
function authInterceptor($rootScope, $q, $window){
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Token token=' + $window.sessionStorage.token;
            }
            return config;
        },
        response: function (response) {
            if (response.status === 401) {
                // handle the case where the user is not authenticated
                
            }
        return response || $q.when(response);
        }
    }; 
}
/* Factory to load users categories names */
function userCategories($http, $rootScope){
    return {
        async: function(){
            var reqCats = {
                method: 'GET',
                url: "http://"+ $rootScope.url + ":" + $rootScope.port + "/user_categories"
            }
            return $http(reqCats);
        }
    }
}

/* Factory to load owners categories names */
function ownerCategories($http, $rootScope){
    return {
        async: function(){
            var reqCats = {
                method: 'GET',
                url: "http://"+ $rootScope.url + ":" + $rootScope.port + "/owner_categories"
            }
            return $http(reqCats);
        }
    }
}
// Factory to load plans for owners
function getPlans($http, $rootScope){
    return{
        async: function(){
            return $http({
                method: 'GET',
                url: "http://"+ $rootScope.url + ":" + $rootScope.port + "/plans"           
            });
        }
    }
}
angular
    .module('inspinia')
    .factory('authInterceptor', authInterceptor)
    .factory('userCategories', userCategories)
    .factory('ownerCategories', ownerCategories)
    .factory('getPlans', getPlans)