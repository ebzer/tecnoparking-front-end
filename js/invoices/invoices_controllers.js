// Invoices main page //
function invoicesMainCtrl($http, $scope){
	$scope.invoices_visitors 	= [];
	$scope.invoices_owners 		= [];

	$http({
		method:'GET',
		url: "http://"+ $scope.url + ":" + $scope.port + "/invoices"
	}).then(function successCallback(response){
		$.each(response.data.invoices, function(key, data){
			if (data.type == "Entry"){
				$scope.invoices_visitors.push(data);
			}
			if (data.type == "Plan"){
				$scope.invoices_owners.push(data);
			}
		});

	});

	this.searchOwner = function searchOwner(country_id){
		alert("Busca cédula: " + country_id);
	}
}
// New invoice page //
function newInvoiceCtrl($http, $scope, getPlans, $window){
	$scope.visitor = false;
	$scope.owner = false;
	$scope.valid = false;
	$scope.value_to_search = "";

	$scope.owner_data = {};
	$scope.plans = {};

	$scope.plan_details = false;

	$scope.$watchGroup(['type_of_search', 'value_to_search'], function(newValues, oldValues){
		// Búsqueda por placa.
		if(newValues[0] == 1){
			if(newValues[1] != null)
			{
				$scope.value_to_search = newValues[1].toUpperCase();
				if (newValues[1].length == 6){
					var regex = new RegExp("[A-Z]{3}[0-9]{3}");
					newValues[1] = newValues[1].toUpperCase();
					if (regex.test(newValues[1])) {
						$scope.valid = true;
						console.log("Match!");
					}else{
						$scope.valid = false;
					}					
				}else{
					$scope.valid = false;
				}
			}
		}
		// Búsqueda por cédula.
		if(newValues[0] == 2){
			if(newValues[1] != null)
			{
				var regex = new RegExp("[0-9]{7,}");
				if (regex.test(newValues[1])){
					$scope.valid = true;
					console.log("Cédula Matched!");
				}else{
					$scope.valid = false;
				}
			}
		}
	});

	this.searchToInvoice = function searchToInvoice(type_of_search, value_to_search){
		if (type_of_search == 1) {
			$http({
				method:'GET',
				url: "http://"+ $scope.url + ":" + $scope.port + "/plates/search_by_plate/" + value_to_search
			}).then(function successCallback(response){
				console.log(response.data);
				$scope.visitor = true;
				$scope.owner = false;
			}, function errorCallback(response){
				alert("Placa no encontrada.");
				$scope.visitor = false;
				$scope.owner = false;
			});
		}
		if (type_of_search == 2) {
			$http({
				method: 'GET',
				url: "http://"+ $scope.url + ":" + $scope.port + "/owners/search_by_country_id/" + value_to_search
			}).then(function successCallback(response){
				$scope.owner = true;
				$scope.visitor = false;
				$scope.owner_data = response.data.owner;
				$scope.owner_data.active = check_validity_date($scope.owner_data.validity.end_validity_date);
				$scope.owner_data.validity.end_validity_date = myToString($scope.owner_data.validity.end_validity_date);
				// Get plans
				$http({
					method:'GET',
					url: "http://"+ $scope.url + ":" + $scope.port + "/plans"
				}).then(function successCallback(response){
					$scope.plans = response.data.plans;
					$scope.plans.selected = $scope.plans[0];
				});			
			}, function errorCallback(response){
				alert("Cédula no encontrada.");
				$scope.visitor = false;
				$scope.owner = false;
			});
		}
	}

	function check_validity_date(date){
		var oneDay = 24*60*60*1000;
		var d = new Date();
		var date = new Date(date);
		var diffDays = Math.round( (d.getTime() - date.getTime())/(oneDay) );
		var response = true;
		if(d.getTime() < date.getTime()){
			response = true;
		}else{
			response = false;
		}
		console.log("Días de diferencia: " + diffDays + ". Response: " + response);
		return response;
	}

	this.validUntil = function validUntil(days, quantity){
		if(days == true){
			var date = new Date();
			var newDate = new Date(date.setTime( date.getTime() + quantity * 86400000 ));
			return myToString(newDate);
		}
	}

	function myToString(date){
		days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
		months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		var date = new Date(date);
		
		function addZero(value){
			if (value < 10) {
				return "0" + value;
			}
			return value;
		}

		function toHours(value){
			if (value > 12) {
				return value - 12;
			}
			if (value == 0) {
				return 12;
			}
			return value;
		}

		function AMPM(value){
			if(value >= 0 && value <= 11){
				return " A.M.";
			}
			if(value >= 12 && value <= 23 ){
				return " P.M.";
			}
		}

		return(days[date.getDay()] + ", " + date.getDate() + " de " + months[date.getMonth()] + " de " + date.getFullYear() + " - " + addZero(toHours(date.getHours())) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds()) + AMPM(date.getHours()) );
	}
	this.invoice = function invoice(owner_id, plan_id){
		$http({
			method: 'POST',
			url: "http://"+ $scope.url + ":" + $scope.port + "/invoices",
			data: {
			    "owner_id": owner_id,
			    "plan_id": plan_id,
			    "current_user_id": $window.sessionStorage.current_user_id,
			    "type_of": "Plan"
			}
		}).then(function successCallback(response){
			console.log(response.data);
		}, function errorCallback(response){
			console.log(response.data);
		});
	}
}

/* Attachments */
angular
    .module('inspinia')
    .controller('invoicesMainCtrl', invoicesMainCtrl)
    .controller('newInvoiceCtrl', newInvoiceCtrl)